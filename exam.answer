# Test 1: Web Design test
# 
# css
# Replicate the layout on the "css & javascript test.mp4"
#     - use the assets provided in "/assets" to get your layout as close as possible to the reference video
#     - disregard font size and family
#     - disregard color accuracy
# 
# javascript
# on the second fold of the layout, replicate the navigation triggers found on the left side of the reference design
#     - Navigate to the frame when the name is clicked
#     - Navigate to adjacent frames when a navigation arrow is clicked (UP / DOWN)
# 
# UX
# use any tool you know to create the transition effects from the reference video
SEE test1/

EXECUTE IN COMMAND-LINE:
npm start

### --- ###

# Scenario: We are launching a sales campaign on our digital shop that will run from January 7 th to January 11 th Pacific Standard Time (-16). The promo is Buy 1 frame, Get 10% off | Buy 2 frames, Get 15% off |Buy 3 frames, Get 20% off. Frame(product) id is 8723 and price is $199.99 .

# Knowing that the cart data schema if as follows

# #schema for the cart
# Cart = [{product}, {product}, ..., {product}]

# #schema for the product
# product = { id: number, original_price: money, discounted_price: I }


# a) What date & time in MNL should the sales campaign start and end?
```
Since Manila time is +8 GMT, The sales campaign would start 16 hours after the day:
MNL/GMT+8 January 7 - 16:00 until January 11 - 16:00
```

# b) Provide algorithm for the discount that will be used for this sales campaign. The discount only applies up to 6 frames.
```javascript
// Taken from test2/ans.js
// Tested using mocha/chai: test2/ans.test.js
function computeDiscount(cart) {
  if (cart.length === 0) {
    return []
  }

  const DISCOUNTED_PROD_ID = 8723
  const DISCOUNT_LIMIT = 6
  // Loop through all cart items
  // Identify discounted items
  const filteredCart = cart.filter(product => product.id === DISCOUNTED_PROD_ID)
  // Preserve undiscounted items
  const remainingCart = cart.filter(product => product.id !== DISCOUNTED_PROD_ID)

  // Count discounted items
  const itemCount = filteredCart.length

  // Compute appropriate discount rate
  let discountRate = 0
  if (itemCount < 1) {
    return cart
  } else if (itemCount < 2) {
    discountRate = 0.1
  } else if (itemCount < 3) {
    discountRate = 0.15
  } else {
    discountRate = 0.2
  }

  // Apply discount
  discountedCart = filteredCart.map((product, i) => {
    if (i < DISCOUNT_LIMIT) {
      const price = product.original_price
      const discount = price * discountRate
      product.discounted_price = price - discount
    }

    return product
  })

  // Return updated cart
  return [...discountedCart, ...remainingCart]
}
```

# c) Explain the logic behind the algorithm composed on b)
```text
1. The algorithm first checks if the cart is empty, if so then return the same cart.
2. It then identifies the products with id 8723, and segregates these products from the rest of the other products
3. It counts the number of 8723 products
3.1 If there are no 8723 products, return the same cart
3.2 If there is 1 8723 product, set the discount rate to 10%
3.3 If there are two 8723 products, set the discount rate to 15%
3.4 If there are three or more 8723 products, set the discount rate to 20%
4. If there are 1 or more 8723 products, it loops through all these products and computes the discount for each product
4.1 If it reaches the seventh product, it applies no discount.
5. The algorithm then returns the discounted products, as well as the products with ids other than 8723
```

# d) From the algorithm on b), provide total amount due when the customer orders 1 up to 6 frames.
```text
1) 179.99
2) 339.98
3) 479.98
4) 639.97
5) 799.96
6) 959.95
```
