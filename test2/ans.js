module.exports = function (cart) {
  if (cart.length === 0) {
    return []
  }

  const DISCOUNTED_PROD_ID = 8723
  const DISCOUNT_LIMIT = 6
  // Loop through all cart items
  // Identify discounted items
  const filteredCart = cart.filter(product => product.id === DISCOUNTED_PROD_ID)
  // Preserve undiscounted items
  const remainingCart = cart.filter(product => product.id !== DISCOUNTED_PROD_ID)

  // Count discounted items
  const itemCount = filteredCart.length

  // Compute appropriate discount rate
  let discountRate = 0
  if (itemCount < 1) {
    return cart
  } else if (itemCount < 2) {
    discountRate = 0.1
  } else if (itemCount < 3) {
    discountRate = 0.15
  } else {
    discountRate = 0.2
  }

  // Apply discount
  discountedCart = filteredCart.map((product, i) => {
    if (i < DISCOUNT_LIMIT) {
      const price = product.original_price
      const discount = price * discountRate
      product.discounted_price = price - discount
    }

    return product
  })

  // Return updated cart
  return [...discountedCart, ...remainingCart]
}
