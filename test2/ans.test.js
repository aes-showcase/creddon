const expect = require("chai").expect
const discount = require("./ans.js")

/*
  const product = {
    id: number,
    original_price: money,
    discounted_price: I
  }
*/
const product8723 = {
  id: 8723,
  original_price: 199.99,
}

function productCreator(prod, price = 100) {
  if (prod === 8723) {
    return JSON.parse(JSON.stringify(product8723))
  }

  return JSON.parse(JSON.stringify({ id: prod, original_price: price }))
}

describe("Exam 2: Clean inputs", () => {
  it("should compute the discount of item 8723 to be 10%", () => {
    const cart = [
      productCreator(8723),
    ]
    const discountedCart = discount(cart)
    expect(discountedCart[0].discounted_price).to.equal(product8723.original_price * .9)
    expect(discountedCart.reduce((total, product) => {
        return total += product.discounted_price
      }, 0)
    ).to.equal(product8723.original_price * .9 * discountedCart.length).to.equal(179.991)
  })

  it("should compute the discount of item 8723 to be 15%", () => {
    const cart = [
      productCreator(8723),
      productCreator(8723),
    ]
    const discountedCart = discount(cart)
    discountedCart.forEach(product => {
      expect(product.discounted_price).to.equal(product.original_price * .85)
    })
    expect(discountedCart.reduce((total, product) => {
        return total += product.discounted_price
      }, 0)
    ).to.equal(product8723.original_price * .85 * discountedCart.length).to.equal(339.983)
  })

  it("should compute the discount of item 8723 to be 20%", () => {
    const cart = [
      productCreator(8723),
      productCreator(8723),
      productCreator(8723),
    ]
    const discountedCart = discount(cart)
    discountedCart.forEach(product => expect(product.discounted_price).to.equal(product.original_price * .80))
    expect(discountedCart.reduce((total, product) => {
        return total += product.discounted_price
      }, 0)
    ).to.equal(product8723.original_price * .80 * discountedCart.length).to.equal(479.97600000000006)
  })

  it("should compute the discount of item 8723 to be 20% for 4 items", () => {
    const cart = [
      productCreator(8723),
      productCreator(8723),
      productCreator(8723),
      productCreator(8723),
    ]
    const discountedCart = discount(cart)
    expect(discountedCart.length).to.equal(4)
    discountedCart.forEach(product => expect(product.discounted_price).to.equal(product8723.original_price * .80))
    expect(discountedCart.reduce((total, product) => {
        return total += product.discounted_price
      }, 0)
    ).to.equal(product8723.original_price * .80 * discountedCart.length).to.equal(639.9680000000001)
  })

  it("should compute the discount of item 8723 to be 20% for 5 items", () => {
    const cart = [
      productCreator(8723),
      productCreator(8723),
      productCreator(8723),
      productCreator(8723),
      productCreator(8723),
    ]
    const discountedCart = discount(cart)
    expect(discountedCart.length).to.equal(5)
    discountedCart.forEach(product => expect(product.discounted_price).to.equal(product8723.original_price * .80))
    expect(discountedCart.reduce((total, product) => {
        return total += product.discounted_price
      }, 0)
    ).to.equal(product8723.original_price * .80 * discountedCart.length).to.equal(799.96)
  })

  it("should compute the discount of item 8723 to be 20% for 6 items", () => {
    const cart = [
      productCreator(8723),
      productCreator(8723),
      productCreator(8723),
      productCreator(8723),
      productCreator(8723),
      productCreator(8723),
    ]
    const discountedCart = discount(cart)
    expect(discountedCart.length).to.equal(6)
    discountedCart.forEach(product => expect(product.discounted_price).to.equal(product.original_price * .80))
    expect(discountedCart.reduce((total, product) => {
        return total += product.discounted_price
      }, 0)
    ).to.equal(959.952)
  })

  it("it should not discount items above the 6th", () => {
    const cart = [
      productCreator(8723),
      productCreator(8723),
      productCreator(8723),
      productCreator(8723),
      productCreator(8723),
      productCreator(8723),
      // 7, 8
      productCreator(8723),
      productCreator(8723),
    ]
    const discountedCart = discount(cart)
    expect(discountedCart.length).to.equal(8)
    discountedCart.forEach((product, i) => {
      if (i < 6) {
        expect(product.discounted_price).to.equal(product.original_price * .80)
      } else {
        expect(product.discounted_price).to.be.undefined
      }
    })
  })
})

describe("Exam 2: Dirty inputs", () => {
  it("should return an empty cart for an empty cart", () => {
    const cart = []
    const empty = discount(cart)
    expect(empty.length).to.equal(0)
    expect(empty).to.deep.equal([])
  })

  it("should compute the discount of item 8723 to be 15% and item 8722 to be zero", () => {
    const cart = [
      productCreator(8723),
      productCreator(8722),
      productCreator(8723),
    ]
    const mixedCart = discount(cart)
    expect(mixedCart.length).to.equal(3)
    expect(mixedCart[0].discounted_price).to.equal(mixedCart[0].original_price * .85)
    expect(mixedCart[2].discounted_price).to.be.undefined
  })

  it("should compute no discounts", () => {
    const cart = [
      productCreator(8721, 200),
      productCreator(8722, 300),
      productCreator(8724),
      productCreator(8725, 250),
    ]
    const undiscountedCart = discount(cart)
    expect(undiscountedCart.length).to.equal(4)
    expect(undiscountedCart[0].original_price).to.equal(200)
    undiscountedCart.forEach(product => {
      expect(product.discounted_price).to.be.undefined
    })
  })

  it("should compute discount of assorted items properly", () => {
    const cart = [
      productCreator(8721, 200),
      productCreator(8722, 300),
      productCreator(8723),
      productCreator(8724),
      productCreator(8725, 250),
      productCreator(8723),
      productCreator(8723),
      productCreator(9999, 999),
      productCreator(8723),
      productCreator(8723),
      productCreator(8723),
      productCreator(1234, 999),
      productCreator(8888, 999),
      productCreator(8723),
      productCreator(8723),
      productCreator(8723),
    ]
    const assortedCart = discount(cart)
    expect(assortedCart.length).to.equal(cart.length)
    const current8723 = []
    assortedCart.forEach((product, i) => {
      if (product.id === 8723) {
        current8723.push(product)
        if (current8723.length < 7) {
          expect(product.discounted_price).to.be.above(0)
        } else {
          expect(product.discounted_price).to.undefined
        }
      } else {
        expect(product.discounted_price).to.undefined
      }
    })
  })
})
