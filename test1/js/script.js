const MAX_ITEMS = 4
const ITEM_DETAILS = [
  {
    title: "Nixplay IRIS",
    titleAnimation: "fade-active",
    subtitle: "Modern Authenticity with Aluminum Classic Bezel",
    sizes: [8],
    media: [
      {
        img: "assets/wifi.svg",
        name: "WIFI",
      }
    ],
    bgColor: "linear-gradient(to bottom, #f1948a,  #fadbd8)",
    bgClass: "products--1",
    className: "iris",
    dots: [
      "grey",
      "white",
      "black",
    ],
  },
  {
    title: "Nixplay Seed",
    titleAnimation: "slide-active",
    subtitle: "Bright color fun with adjustable angle.",
    sizes: [8, 10, 13],
    media: [
      {
        img: "assets/wifi.svg",
        name: "WIFI",
      }
    ],
    bgColor: "linear-gradient(to bottom left,  #85C1E9, #fff)",
    bgClass: "products--2",
    className: "seed",
    dots: [
      "blue",
      "black",
      "orange",
    ],
  },
  {
    title: "Nixplay EDGE",
    titleAnimation: "slide-active",
    subtitle: "Basic and black with high-resolution display.",
    sizes: [8],
    media: [
      {
        img: "assets/wifi.svg",
        name: "WIFI",
      },
      {
        img: "assets/flash-drive.svg",
        name: "USB",
      },
      {
        img: "assets/memory-card.svg",
        name: "CARD",
      },
    ],
    bgColor: "linear-gradient(to bottom,  #85C1E9, #82E0AA, #D5F5E3)",
    bgClass: "products--3",
    className: "edge",
    dots: [
      "black",
    ],
  },
  {
    title: "Nixplay Original",
    titleAnimation: "slide-active",
    subtitle: "Entry series with larger screen size options",
    sizes: [15],
    media: [
      {
        img: "assets/wifi.svg",
        name: "WIFI",
      },
      {
        img: "assets/flash-drive.svg",
        name: "USB",
      },
      {
        img: "assets/memory-card.svg",
        name: "CARD",
      },
    ],
    bgColor: "",
    bgClass: "products--4",
    className: "original",
    dots: [
      "black",
    ],
  },
]

let currentIndex = 0
let prevIndex = 0

// Pure javascript fadeIn
// https://stackoverflow.com/a/23244622
function fadeIn(el, time) {
  el.style.opacity = 0;

  let last = +new Date();
  const tick = function() {
    el.style.opacity = +el.style.opacity + (new Date() - last) / time;
    last = +new Date();

    if (+el.style.opacity < 1) {
      (window.requestAnimationFrame && requestAnimationFrame(tick)) || setTimeout(tick, 16);
    }
  };

  tick();
}

function fadeOut(el, time) {
  el.style.opacity = 1;

  let last = +new Date();
  const tick = function() {
    el.style.opacity = +el.style.opacity - (new Date() - last) / time;
    last = +new Date();

    if (+el.style.opacity > 0) {
      (window.requestAnimationFrame && requestAnimationFrame(tick)) || setTimeout(tick, 16);
    }
  };

  tick();
}

const navItems = Array.from(document.querySelectorAll(".products__nav-item"))
const arrow = {
  prev: document.querySelector(".products__prev"),
  next: document.querySelector(".products__next"),
}
const photos = Array.from(document.querySelectorAll(".products__photo-box"))
const details = {
  // title: document.querySelector(".products__title"),
  // title: Array.from(document.querySelectorAll(".products__title")),
  // subtitle: document.querySelector(".products__subtitle"),
  headers: Array.from(document.querySelectorAll(".products__header-group")),
  sizes: document.querySelector(".products__sizes"),
  media: document.querySelector(".products__media"),
}

const setProduct = index => {
  if (index >= MAX_ITEMS) {
    return
  }

  if (index < 0) {
    return
  }

  prevIndex = currentIndex
  currentIndex = index
  setBackground(index)
  setActiveNav(index)
  setArrows(index)
  setDots(index)
  setPhoto(index)
  setDetails(index)
}

const setBackground = index => {
  const products = document.querySelector(".products")
  products.classList.remove("products--1", "products--2", "products--3", "products--4")
  products.classList.add(ITEM_DETAILS[index].bgClass)
  
}

const setActiveNav = index => {
  navItems.forEach((item, i) => {
    item.classList.remove("active")
    if (index === i) {
      item.classList.add("active")
    }
  })
}

const setArrows = index => {
  arrow.prev.classList.remove("disabled")
  arrow.next.classList.remove("disabled")

  if (index < 1) {
    arrow.prev.classList.add("disabled")
  } else if (index > 2) {
    arrow.next.classList.add("disabled")
  }
}

const setDots = index => {
  const { dots } = ITEM_DETAILS[index]
  const dotsList = document.querySelector(".products__dots-list")
  let newItem = document.querySelector(".products__dots-item")
    .cloneNode(true)

  Array.from(dotsList.children).forEach(child => {
    fadeOut(child, 200)
    dotsList.removeChild(child)
  })
  /*
  while(dotsList.firstChild) {
    dotsList.removeChild(dotsList.firstChild)
  }
  */

  dots.forEach((dot, i) => {
    newItem.classList.remove(
      "products__dots-item--grey",
      "products__dots-item--white",
      "products__dots-item--black",
      "products__dots-item--blue",
      "products__dots-item--orange",
      "active",
    )
    newItem.classList.add("products__dots-item--" + dot)
    dotsList.append(newItem)

    if (dots.length > 1) {
      if (i === 1) {
        newItem.classList.add("active")
        fadeIn(newItem, 20)
      } else {
        fadeIn(newItem, 200)
      }
    } else {
      if (i === 0) {
        newItem.classList.add("active")
      }
    }

    newItem = newItem.cloneNode(true)
  })
}

const setPhoto = index => {
  photos.forEach((photo, i) => {
    photo.classList.remove("active")
    if (index === i) {
      photo.classList.add("active")
    }
  })
}

// SLIDER SETUP
let x = 0,
    container = $('.products__header'),
    items = container.find('.products__header-group'),
    containerHeight = 0,
    numberVisible = 1

if(!container.find('.products__header-group:first').hasClass("first")){
  container.find('.products__header-group:first').addClass("first");
}
if(!container.find('.products__header-group:last').hasClass("last")){
  container.find('.products__header-group:last').addClass("last");
}

items.each(function(){
  if(x < numberVisible){
    containerHeight = containerHeight + $(this).outerHeight();
    x++;
  }
});

container.css({ height: containerHeight, overflow: "hidden" });

const setDetails = index => {
  const { title, subtitle, sizes, media } = ITEM_DETAILS[index]

  /*
  details.title.textContent = title
  details.subtitle.textContent = subtitle

  details.headers.forEach((header, i) => {
    header.classList.remove("active")
  })
  details.headers[index].classList.add("active")
  */

  function vertCycle(prev, current) {
    if (prev === current) return

    const offset = containerHeight * index * -1 + "px"
    const options = {
      transform: `translateY(${offset})`,
      transition: "all .2s",
    }

    if (prev < current) {
      items.each(function () {
        $(this).css(options)
      })
    } else {
      items.each(function () {
        $(this).css(options)
      })
    }
  }
  vertCycle(prevIndex, currentIndex)

  /*
  details.title.forEach((title, i) => {
    title.classList.remove("active")
    if (index === i) {
      title.classList.add("active")
    }
  })
  */

  let clonedNode = null
  const prevSizes = ITEM_DETAILS[prevIndex].sizes

  // fadeIn(details.sizes, 200)
  while (details.sizes.firstChild) {
    if (details.sizes.firstChild.nodeName === "SPAN") {
      clonedNode = details.sizes.firstChild.cloneNode(true)
    }
    details.sizes.removeChild(details.sizes.firstChild)
  }
  sizes.forEach((size, i) => {
    clonedNode.classList.remove("active")
    if (i === 0) {
      clonedNode.classList.add("active")
    }
    clonedNode.textContent = size + "\""
    details.sizes.appendChild(clonedNode)
    if (i > prevSizes.length - 1) {
      fadeIn(clonedNode, 200)
    }
    clonedNode = clonedNode.cloneNode(true)
  })

  const prevMedia = ITEM_DETAILS[prevIndex].media
  while (details.media.firstChild) {
    if (details.media.firstChild.nodeName === "DIV") {
      clonedNode = details.media.firstChild.cloneNode(true)
      fadeOut(details.media.firstChild, 200)
    }
      details.media.removeChild(details.media.firstChild)
  }
  media.forEach((medium, i) => {
    clonedNode.querySelector("img").src = medium.img
    clonedNode.querySelector("p").textContent = medium.name
    details.media.appendChild(clonedNode)
    if (i > prevMedia.length - 1) {
      fadeIn(clonedNode, 200)
    }
    clonedNode = clonedNode.cloneNode(true)
  })
}

navItems.forEach((item, i) => {
  item.addEventListener("click", () => {
    setProduct(i)
  })
})

arrow.prev.addEventListener("click", () => {
  setProduct(currentIndex - 1)
})

arrow.next.addEventListener("click", () => {
  setProduct(currentIndex + 1)
})

setProduct(0)
